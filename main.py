import requests
import json
from flask import jsonify


def get_location(place):
    # cache.clear()
    r = requests.request("GET", url="http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?SingleLine=" +
                         place + "&outFields=PlaceName,Type,Place_Addr,City,Region,Country&maxLocations=1&forStorage=false&f=pjson")
    data = r.json()
    resposta = {}
    resposta.update(
        status=True,
        location=data['candidates'][0]['address'],
        lat=round(data['candidates'][0]['location']['y'], 3),
        lon=round(data['candidates'][0]['location']['x'], 3)
    )
    return resposta


def pluvion_chatbot_location(request):

    json_req = request.args.to_dict(flat=False)

    # url/?lat=value&lon=value
    location = (json_req['location'])[0]

    if request.method == 'OPTIONS':
        # Allows GET requests from any origin with the Content-Type
        # header and caches preflight response for an 3600s
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }

        return ('', 204, headers)

    # Set CORS headers for the main request
    headers = {
        'Access-Control-Allow-Origin': '*'
    }

    try:
        data = get_location(location)
    except:
        data = {"status": False}

    return (jsonify(data), 200, headers)
